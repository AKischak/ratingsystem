<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExpertGroups extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('expert_groups', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer("expert_id")->unsigned();
            $table->integer("group_id")->unsigned();

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->foreign('expert_id')->references('id')->on('experts')->onDelete('cascade');
            $table->foreign('group_id')->references('id')->on('examination_group')->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('expert_groups');
	}

}
