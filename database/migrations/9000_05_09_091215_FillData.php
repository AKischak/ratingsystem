<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class FillData extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('education_types')->insert(array('id' => 1, 'name' => 'Начальное', 'order' => 1));
        DB::table('education_types')->insert(array('id' => 2, 'name' => 'Неполное среднее', 'order' => 2));
        DB::table('education_types')->insert(array('id' => 3, 'name' => 'Среднее', 'order' => 3));
        DB::table('education_types')->insert(array('id' => 4, 'name' => 'Базовое высшее', 'order' => 5));
        DB::table('education_types')->insert(array('id' => 5, 'name' => 'Полное высшее', 'order' => 6));
        DB::table('education_types')->insert(array('id' => 6, 'name' => 'Послевузовское', 'order' => 7));

        DB::table('users')->insert(array(
            'id'=> 1,
            'first_name' => "Администратор",
            'last_name' => "A",
            'middle_name' => 'A',
            'login' => 'admin',
            'password' => '$2y$10$QAlv5iXiqMLgXofhrQP.A.VCzQ0uNGp6H09cSyNJYvL6kCJ8wjE6G', // admin
            'confirmed' => true
        ));

        DB::table('roles')->insert(array('id' => 1, 'name' => 'Администратор'));
        DB::table('roles')->insert(array('id' => 2, 'name' => 'Пользователь'));
        DB::table('roles')->insert(array('id' => 3, 'name' => 'Преподаватель'));
        DB::table('roles')->insert(array('id' => 4, 'name' => 'Эксперт'));

        DB::table('user_roles')->insert(array('user_id' => '1', "role_id" => 1));


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::delete('delete from user_roles');
        DB::delete('delete from roles');
    }

}
