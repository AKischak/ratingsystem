<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TeacherRating extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('teacher_rating', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('attestation')->unsigned();

            $table->foreign('attestation')->references('id')->on('attestations')->onDelete('cascade');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('teacher_rating');
	}

}
