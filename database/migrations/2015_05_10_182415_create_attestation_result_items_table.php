<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttestationResultItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attestation_result_items', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('unit')->unsigned();
            $table->integer('expert_id')->unsigned();
            $table->integer("attestation_result")->unsigned();
            $table->integer("value");

            $table->foreign('unit')->references('id')->on('post_requirements')->onDelete('cascade');
            $table->foreign('expert_id')->references('id')->on('experts')->onDelete('cascade');
            $table->foreign('attestation_result')->references('id')->on('attestation_results')->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attestation_result_items');
	}

}
