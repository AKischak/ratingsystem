<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttestationResultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attestation_results', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('attestation_id')->unsigned();
            $table->enum('status', array('positive', 'middle', 'negative'));
            $table->integer('expert_id')->unsigned();
            $table->decimal('appointment', 5, 5);

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->foreign('attestation_id')->references('id')->on('attestations')->onDelete('cascade');
            $table->foreign('expert_id')->references('id')->on('experts')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attestation_results');
	}

}
