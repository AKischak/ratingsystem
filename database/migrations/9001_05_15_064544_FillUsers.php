<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::table('users')->insert(array(
            'id'=> 2,
            'first_name' => "Вася",
            'last_name' => "Петров",
            'middle_name' => 'A',
            'login' => 'petrov',
            'password' => '$2y$10$CDFa.oFgEHIQL8L8b9ei9eVdOtNApe8KukJvjSDfz.6G62DoJ/pa6', // 1111
            'confirmed' => true
        ));
        DB::table('users')->insert(array(
            'id'=> 3,
            'first_name' => "Александр",
            'last_name' => "Николаев",
            'middle_name' => 'A',
            'login' => 'nikol',
            'password' => '$2y$10$CDFa.oFgEHIQL8L8b9ei9eVdOtNApe8KukJvjSDfz.6G62DoJ/pa6', // 1111
            'confirmed' => true
        ));
        DB::table('users')->insert(array(
            'id'=> 4,
            'first_name' => "Валерий",
            'last_name' => "Дубровин",
            'middle_name' => 'A',
            'login' => 'dubrovin',
            'password' => '$2y$10$CDFa.oFgEHIQL8L8b9ei9eVdOtNApe8KukJvjSDfz.6G62DoJ/pa6', // 1111
            'confirmed' => true
        ));

        DB::table('user_roles')->insert(array('user_id' => '2', "role_id" => 4));
        DB::table('user_roles')->insert(array('user_id' => '3', "role_id" => 4));
        DB::table('user_roles')->insert(array('user_id' => '4', "role_id" => 3));


        DB::table('teachers')->insert(array('user_id' => '4'));
        DB::table('experts')->insert(array('user_id' => '2'));
        DB::table('experts')->insert(array('user_id' => '3'));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::delete('delete from experts');
        DB::delete('delete from teachers');
        DB::delete('delete from users');
	}

}
