var Laravel = {};
Laravel.Drawing = {};


Laravel.Drawing.Manager = function(){
    var self = this;
    self.LoopId = null;
    self.Handlers = [];
    self.Interval = 100;
    self.RepeatCount = 100;


    self.constructor = function(){
        self.ReplaceCanvas();
        $(window).resize(function(){
            self.ReplaceCanvas();
        });

        var ctx = self.GetContext();
        ctx.strokeStyle="#FF0000";
    };

    self.ReplaceCanvas = function(){
        var height = $("body").height();
        var width = $("body").width();
        console.log(height+", "+width);
        var html = '<canvas id="draw-field" height="##HEIGHT##" width="##WIDTH##"></canvas>';
        html = html.replace("##HEIGHT##", height).replace("##WIDTH##", width);
        $("#canvas-wrap").html(html);
    };

    self.GetContext = function(){
        var canvas = $("#draw-field")[0];
        return canvas.getContext("2d");
    };

    self.StartLoop = function(){
        self.counter = 0;
        self.LoopId = setInterval(function(){
            for(var i in self.Handlers){
                self.Handlers[i]();
            }
            self.counter++;
            if(self.counter >= self.RepeatCount){
                self.StopLoop();
            }
        }, self.Interval);
    };

    self.StopLoop = function(){
        if(self.LoopId){
            clearInterval(self.LoopId);
        }
    };

    self.StartOnce = function(){
        for(var i in self.Handlers){
            self.Handlers[i]();
        }
    };

    self.AddHandler = function(func){
        self.Handlers.push(func);
    }

    self.constructor();
}

$(document).ready(function(){
    var processor = new Laravel.Drawing.Manager();
    processor.RepeatCount = 9000;
    processor.Interval = 5;
    processor.AddHandler(function(){
        var ctx = processor.GetContext();

        ctx.beginPath();
        ctx.strokeStyle = GetRandomColor();
        ctx.moveTo(500, 300);
        ctx.lineTo(Math.sin(processor.counter/8)*500+500, Math.cos(processor.counter/5)*300+300);
        ctx.stroke();
    });

    processor.StartLoop();
    //processor.StopLoop();
});

function GetRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}