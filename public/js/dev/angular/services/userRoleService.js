

laravel.service("userRole", function($http, $q){
    return {
        updateRoles: function(userId, roles){
            return $http.post("/userRoleUpdate", {
                userId: userId,
                roles: roles
            });
                /*.success(function (data, success) {
                console.log("update success: "+data);
            }).error(function (data) {
                console.log("error while updating roles: "+data)

            })*/
        },
        confirmUser: function(userId){
            return $http.post("/tryConfirm", {
                userId: userId
            });
        }
    }
});