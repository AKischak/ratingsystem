/**
 * Created by Андрей on 14.05.2015.
 */

laravel.service("postService", function($http, $q){
    return {
        updatePostRequirements: function(postId, requirements){
            return $http.post("/post-req-update", {
                postId: postId,
                requirements: requirements
            });
        }
    }
});