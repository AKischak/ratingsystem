

laravel.service("expertGroup", function($http, $q){
    return {
        updateGroupExperts: function(groupId, expertsToAdd){
            return $http.post("/save-group-experts", {
                groupId: groupId,
                experts: expertsToAdd
            });
        },
        updateGroupAttestationTeachers: function(groupId, attest){
            return $http.post("/save-attestation", {
                groupId: groupId,
                attestation: attest
            });
        }
    }
});