/**
 * Created by Андрей on 10.05.2015.
 */




laravel.controller("registerCtrl", function($scope, userRole){
    $scope.rolesRaw = [];
    $scope.roles =[];
    $scope.userRoles = [];

    _.each(window.global_roles, function (role) {
        $scope.roles.push({
            checked: false,
            roleName: role.name,
            id: role.id
        });
    });

    $scope.$watch("roles", function () {
        var filtered = _.filter($scope.roles, function(i){ return i.checked; });
        $scope.rolesRaw = _.map(filtered, function(item){ return item.id; }).join();
        console.log($scope.roles);
    },true);


    $scope.saveRoles = function(){
        var filtered = _.filter($scope.userRoles, function(i){ return i.isSelected; });
        var roles = JSON.stringify(_.map(filtered, function(item){ return item.id; }));

        userRole.updateRoles(window.user.id, roles);
    }

});