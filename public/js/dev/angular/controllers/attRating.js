/**
 * Created by Андрей on 25.05.2015.
 */


laravel.controller("attRatingCtrl", function($scope, $compile){
    console.log(window.req);

    $scope.requirements = [];
    /*[
        {
            name: "Социальная работа",
            value: 0,
            min: 0,
            max: 10,
            threshold: 5
        }
    ];*/

    $scope.init = function(){
        _.each(window.req, function(item){
            $scope.requirements.push({
                name: item.name,
                value: item.min_value,
                min: item.min_value,
                max: item.max_value,
                threshold: (+item.threshold).toFixed(0)
            });
        });
    }

    $scope.init();

});