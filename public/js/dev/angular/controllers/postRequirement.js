/**
 * Created by Андрей on 12.05.2015.
 */

var REQUIREMENT_NEW_TYPE = "h4n3iqu5";
var REQUIEREMENT_STORAGE_TYPE = "nliqrudfy";

var EDIT_TYPE_NEW = "sdfkjqb3";
var EDIT_TYPE_UPDATE =";wlrjfmpo";

laravel.controller("postRequirementCtrl", function($scope, $compile, $timeout, postService){
    $scope.storageRequirements = [];
    $scope.editDirectiveScope = null;
    $scope.saveStatus = 1;
    $scope.isChanged = false;
    $scope.postId = window.postId;

    $scope.initRequirements = function(){
        var index = 0;
        _.each(window.requirements, function(req){

            $scope.storageRequirements.push({
                id: req.id,
                name: req.name,
                min: req.min_value,
                max: req.max_value,
                valueType: req.value_type,
                threshold: (+req.threshold).toFixed(1),
                isNew: false,
                isDeleted: false,
                isUpdated: false,
                storageIndex: index
            });
            $scope.addInfoPanel(index);
            index++;
        });
    };

    $scope.addNewPostRequirement = function(requirement){
        $scope.storageRequirements.push(requirement);
        $scope.isChanged = true;
        var index = $scope.storageRequirements.length - 1;
        requirement.storageIndex = index;

        $scope.addInfoPanel(index);
    };

    $scope.addInfoPanel = function(index){
        var html = "<ng-requirement-info req-id='##INDEX##'></ng-requirement-info>";
        html = html.replace("##INDEX##", index);
        $("#requirements-container").append($compile(html)($scope));
    };

    $scope.updateRequirement = function(requirement){
        if($scope.editDirectiveScope){
            $scope.editDirectiveScope.fillData(requirement, true);
        }
    };

    $scope.saveChanges = function(requirement){
        $scope.isChanged = true;
        $scope.storageRequirements[requirement.storageIndex] = requirement;
        requirement.isUpdated = true;
    };

    $scope.deleteRequirement = function(requirement){
        requirement.isDeleted = true;
        $scope.isChanged = true;
    };

    $scope.SaveToServer = function(){
        if($scope.saveStatus != 1) return;
        $scope.saveStatus = 2;

        var promise = postService.updatePostRequirements($scope.postId, JSON.stringify($scope.storageRequirements));

        promise.then(function(){
            $scope.saveStatus = 3;
            $scope.isChanged = false;

            $timeout(function(){
                $scope.saveStatus = 1;
            }, 1000);
        });
        $scope.storageRequirements = _.filter($scope.storageRequirements, function(el){
            return !el.isDeleted;
        });
        _.each($scope.storageRequirements, function(item, index){
            item.isNew = false;
            item.isUpdated = false;
        });
    }

    $scope.initRequirements();
});