/**
 * Created by Андрей on 11.05.2015.
 */


laravel.controller("attestationCtrl", function($scope, $compile, $timeout, $rootScope, expertGroup){
    $scope.group = window.group;

    $scope.Teachers = {};
    $scope.Posts = {};
    $scope.Attestations = {};

    $scope.selectedTeacher = {};
    $scope.selectedPost = {};

    $scope.teachersToAdd = [];
    $scope.postsToAdd = [];

    $scope.isChanged = false;
    $scope.canAdd = false;

    $scope.saveStatus = 1;

    $scope.addAttestation = function(){
        if(!$scope.selectedTeacher.id) return;
        if(!$scope.selectedPost.id) return;
        $scope.isChanged = true;

        $scope.addNewPanel($scope.selectedTeacher.id, $scope.selectedPost.id);
        $scope.refreshCanAddAttestation();
    };

    $scope.addNewPanel = function (teacherId, postId) {
        var attId = _.random(99999, 9999999)
        $scope.Attestations[attId] = {
            teacherId: teacherId,
            postId: postId,
            id: attId,
            isNew: true,
            isDeleted: false
        };
        $scope.addPanel(attId);
    },

    $scope.addPanel = function(attId){
        $attestation = $scope.Attestations[attId];
        var html = "<ng-attestation teacher='Teachers[#TH_ID#]' post='Posts[##POST_ID##]' attestation = 'Attestations[##A_ID##]'></ng-attestation>";
        html = html.replace('#TH_ID#', $attestation.teacherId);
        html = html.replace('##POST_ID##', $attestation.postId);
        html = html.replace('##A_ID##', $attestation.id);
        var dom = $compile(html)($scope);
        $("#users-wrapper").append(dom);
        $scope.refreshCanAddAttestation();

    };

    $scope.refreshCanAddAttestation = function(){
        $scope.canAdd = true;
        if(!$scope.selectedTeacher.id || !$scope.selectedPost.id) {
            $scope.canAdd = false;
            return;
        }
        var elem = _.find($scope.Attestations, function(item){
            return  item.teacherId == $scope.selectedTeacher.id &&
                    item.postId == $scope.selectedPost.id &&
                    !item.isDelete;
        });
        if(elem){
            $scope.canAdd = false;
        }
    };

    $scope.selectTeacher = function(id){
        $scope.selectedTeacher  = _.find($scope.teachersToAdd, function(el){
            return el.id == id;
        });
        $scope.refreshCanAddAttestation();
    };

    $scope.selectPost = function(id){
        $scope.selectedPost  = _.find($scope.postsToAdd, function(el){
            return el.id == id;
        });
        $scope.refreshCanAddAttestation();
    };

    $scope.removeItem = function(attId){
        $scope.isChanged = true;

        if($scope.Attestations[attId]){
            if($scope.Attestations[attId].isNew){
                delete $scope.Attestations[attId];
            }else{
                $scope.Attestations[attId].isDeleted = true;
            }
        }
        $scope.$broadcast("attestation.safe.delete", attId);
        $scope.sortTeachers();
        $scope.refreshCanAddAttestation();
    };

    $scope.sortTeachers = function(){
        $scope.teachersToAdd = _.sortBy($scope.teachersToAdd, function(el){
            return el.id;
        })
    };

    $scope.saveAttestation = function(){
        if($scope.saveStatus != 1) return;
        $scope.saveStatus = 2;
        $scope.isChanged = false;

        var toSave = _.filter($scope.Attestations, function(e){ return e.isNew || e.isDeleted; });

        var promise = expertGroup.updateGroupAttestationTeachers($scope.group.id, JSON.stringify(toSave));
        promise.then(function(data){
            _.each($scope.Attestations, function(el){
                if(el.isDeleted || el.isNew){
                    $scope.$broadcast('before.attestation.removed', el.id);
                    delete $scope.Attestations[el.id];
                }
            });

            _.each(data.data, function(newEl){
                $scope.Attestations[newEl.id] = GetJSObjectFromDBObject(newEl);
                $scope.addPanel(newEl.id);
            });

            $scope.saveStatus = 3;
            $timeout(function(){
                $scope.saveStatus = 1;
            }, 1000);
        });
    };

   // INIT LIST FILL
    _.each(window.allTeachers, function(el){
        $scope.Teachers[el['id']] = el;
        $scope.teachersToAdd.push({
            id: el.id,
            name: el.user.first_name+", "+el.user.last_name
        });
    });
    _.each(window.posts, function(el){
        $scope.Posts[el['id']] = el;
        $scope.postsToAdd.push({
            id: el.id,
            name: el.name
        })
    });
    _.each(window.attestations, function(item){
        $scope.Attestations[item.id] = GetJSObjectFromDBObject(item);
        $scope.addPanel(item.id);
    });

});


function GetJSObjectFromDBObject(dbObject){
    return {
        id: dbObject.id,
        teacherId: dbObject.teacher_id,
        postId: dbObject.post_id,
        groupId: dbObject.group_id,
        finished: dbObject.finished
    };
}