/**
 * Created by Андрей on 11.05.2015.
 */


laravel.controller("expertGroupCtrl", function($scope, $compile, $timeout, expertGroup) {
    $scope.message = '1';
    $scope.selectedExpert = {};
    $scope.Experts = {};
    $scope.expertsToAdd = [];
    $scope.isChanged = false;
    $scope.expertsToSave = [];
    $scope.group = window.group;
    $scope.saveStatus = 1;

    $scope.addExpert= function(){
        if(!$scope.selectedExpert.id) return;
        $scope.isChanged = true;
        $scope.addPanel($scope.selectedExpert.id);
        $scope.selectedExpert = {};
    }

    $scope.addPanel = function(id){
        var html = "<ng-expert expert='Experts[#USER_ID#]'></ng-expert>";
        html = html.replace('#USER_ID#', id);
        var dom = $compile(html)($scope);
        $("#users-wrapper").append(dom);

        $scope.expertsToAdd = _.filter($scope.expertsToAdd, function(el){
            return el.id != id;
        });
        $scope.expertsToSave.push(id);
    }


    $scope.selectExpert = function(id){
        $scope.selectedExpert  = _.find($scope.expertsToAdd, function(el){
            return el.id == id;
        });
    }
    $scope.removeItem = function(id, dom){
        $scope.isChanged = true;
        var exp = _.find($scope.Experts, function(el){
            return el.user_id == id;
        });
        $scope.expertsToSave = _.filter($scope.expertsToSave, function(i){ return i != exp.id; });

        $scope.expertsToAdd.push({
            id: exp.id,
            name: exp.user.first_name+", "+exp.user.last_name
        });
        $scope.sortExperts();
    }

    $scope.sortExperts = function(){
        $scope.expertsToAdd = _.sortBy($scope.expertsToAdd, function(el){
            return el.id;
        })
    }

    $scope.saveExperts = function(){
        if($scope.saveStatus != 1) return;
        $scope.saveStatus = 2;
        $scope.isChanged = false;

        //$scope.expertsToSave = _.uniq($scope.expertsToSave);
        var promise = expertGroup.updateGroupExperts($scope.group.id, JSON.stringify($scope.expertsToSave));
        promise.then(function(){
            $scope.saveStatus = 3;

            $timeout(function(){
                $scope.saveStatus = 1;
            }, 1000);
        });
    }

    // -- fill

    _.each(window.allExperts, function(el){
        $scope.Experts[el['id']] = el;
        $scope.expertsToAdd.push({
            id: el.id,
            name: el.user.first_name+", "+el.user.last_name
        })
    });

    _.each(window.groupExperts, function(el){
        $scope.addPanel(el.id);
    });
});