

laravel.controller("userCtrl", function($scope, $timeout, userRole){
    $scope.message = "fsdfsdfsdf";
    $scope.userRolesResult = [];
    $scope.roles = window.global_roles;
    $scope.status = 1;
    $scope.userRoles = [];

    _.each($scope.roles, function (role) {
        $scope.userRoles.push({
            isSelected: _.size(_.filter(window.user_roles, function(el){
                return el.id == role.id;
            })) > 0,
            name: role.name,
            id: role.id
        });
    });

    $scope.$watch("userRoles", function () {
        var filtered = _.filter($scope.userRoles, function(i){ return i.isSelected; });
        $scope.userRolesResult = JSON.stringify(_.map(filtered, function(item){ return item.id; }));
    },true);

    $scope.itemName = "";
    $scope.pushItem = function(){
        $scope.userRoles.push({
            isSelected: false,
            name: $scope.itemName,
            id: 0
        });
        console.log($scope.userRoles);
    }

    $scope.saveRoles = function(){
        if($scope.status != 1) return;
        $scope.status = 2;

        var filtered = _.filter($scope.userRoles, function(i){ return i.isSelected; });
        var roles = JSON.stringify(_.map(filtered, function(item){ return item.id; }));


        var promise = userRole.updateRoles(window.user.id, roles);

        promise.then(function(){
            $scope.status = 3;
            $timeout(function(){
                $scope.status = 1;
            }, 1000);
        });

    }

});