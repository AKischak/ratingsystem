/**
 * Created by Андрей on 10.05.2015.
 */


laravel.controller("confirmUserCtrl", function($scope, userRole){
    $scope.allWishes = {};
    $scope.roles = {};
    $scope.userRoles = [];
    $scope.userStatus = {}

    _.each(window.allRoles, function (role) {
        $scope.roles[role['id']] = role['name'];
    });

    _.each(window.allWishes, function (w) {
        var wroles = w.roles.length > 0 ? w.roles.split(',') : [];
        $scope.userStatus[w['user_id']] = 1;
        if(wroles.length > 0){
            $scope.allWishes[w['user_id']] = _.map(wroles, function(el){
                return {
                    id: el,
                    name: $scope.roles[el],
                    checked: false
                }
            });
        }else{
            $scope.allWishes[w['user_id']] = [];
        }
    });

    $scope.saveRoles = function(id){
        if($scope.userStatus[id] != 1) return;
        $scope.userStatus[id] = 2;


        var filtered = _.filter($scope.allWishes[id], function(el){ return el.checked; });
        var roles = JSON.stringify(_.map(filtered, function(item){ return item.id; }));

        var successCount = 0;
        var handler = function(){
            if(successCount == 1){
                $scope.userStatus[id] = 3;
            }
        }

        var promise = userRole.updateRoles(id, roles);
        var promise2 = userRole.confirmUser(id);

        promise.then(function(){
            successCount++;
            handler();
        });
        promise2.then(function(){
            successCount++;
            handler();
        });


    }

});