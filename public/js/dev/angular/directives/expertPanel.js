/**
 * Created by Андрей on 11.05.2015.
 */


laravel.directive('ngExpert', function(){
    var linkFunction = function(scope, element, attributes) {
        scope.element = element;
        scope.user = scope.expert.user;
    };

    return {
        restrict: "E",
        templateUrl: "/ng-user-block",
        link: linkFunction,
        scope: {
            expert: '=expert'
        },
        controller: function($scope){
            $scope.remove = function(){
                $scope.element.remove();
                $scope.$parent.removeItem($scope.user.id);
            }
        }
    }
});

