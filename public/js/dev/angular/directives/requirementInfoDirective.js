/**
 * Created by Андрей on 13.05.2015.
 */

laravel.directive('ngRequirementInfo', function(){

    var link = function(scope, element, attributes){
        scope.reqId = attributes.reqId;
        scope.dom = element;
        scope.requirement = scope.$parent.storageRequirements[attributes.reqId];
    }

    return {
        restrict: "E",
        link: link,
        scope: {},
        controller: function($scope){
            $scope.message = 'Info';
            $scope.edit = function(){
                $scope.$parent.updateRequirement($scope.requirement);
            }
            $scope.remove = function(){
                $scope.$parent.deleteRequirement($scope.requirement);
                $scope.dom.remove();
            }
        },
        templateUrl: '/ng-requirement-info'
    }
});