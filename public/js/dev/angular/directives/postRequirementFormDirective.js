/**
 * Created by Андрей on 12.05.2015.
 */

laravel.directive("ngPostRequirement", function () {
    var handlerLink = function (scope, element, attributes) {
        scope.$parent.editDirectiveScope = scope;
        scope.element = element;
    };

    return {
    restrict: "E",
    link: handlerLink,
    scope: {},
    controller: function($scope){
        $scope.index = 1;
        $scope.requirement = null;
        $scope.editType = 1;

        $scope.resetToDefaults = function(resetChanges){
            if(resetChanges){
                $scope.requirement.name = $scope.orignalRequirement.name;
                $scope.requirement.min = $scope.orignalRequirement.min;
                $scope.requirement.max = $scope.orignalRequirement.max;
                $scope.requirement.threshold = $scope.orignalRequirement.threshold;
            }

            $scope.requirement = {
                name: "Требование "+$scope.index,
                min: 0,
                max: 10,
                threshold: 5.0,
                valueType: 'number',
                isNew: true,
                isDeleted: false,
                isUpdated: false
            };
            $scope.editType = 1;
            if(!resetChanges){
                $scope.index++;
            }
        };

        $scope.processData = function(postReqForm){
            if(postReqForm.$valid){
                if($scope.editType == 1){
                    $scope.$parent.addNewPostRequirement($scope.requirement);
                }else if($scope.editType == 2){
                    $scope.$parent.saveChanges($scope.requirement);
                }

                $scope.resetToDefaults();
            }else{
                console.log("not valid");
            }
        }

        //$scope.fields = ['minValue', 'maxValue', 'thresholdValue'];
        $scope.$watchCollection('[requirement.min, requirement.max, requirement.threshold, requirement.valueType]', function(){
            $scope.recalculateAllValidation();
        });


        $scope.recalculateAllValidation = function () {
            var valid = true;
            if($scope.requirement.min >= $scope.requirement.max || $scope.requirement.min < 0){
                valid = false;
            }
            $scope.postReqForm.minValue.$setValidity('Custom', valid);

            valid = true;
            if($scope.requirement.max <= $scope.requirement.min ||  $scope.requirement.max <= 0){
                valid = false;
            }
            $scope.postReqForm.maxValue.$setValidity('Custom', valid);

            valid = true;
            if($scope.requirement.valueType == 'number' || !$scope.requirement.valueType){
                if($scope.requirement.threshold < $scope.requirement.min || $scope.requirement.threshold > $scope.requirement.max){
                    valid = false;
                }
            }

            $scope.postReqForm.thresholdValue.$setValidity('Interval', valid);
        }

        $scope.fillData = function(req, focus){
            $scope.orignalRequirement = jQuery.extend({}, req);
            $scope.requirement = req;

            if(focus){
                $('html, body').animate({
                    scrollTop: $scope.element.context.offsetParent.clientHeight
                }, 1000);
            }
            $scope.editType = 2;
        }

        $scope.abortEdit = function(){
            $scope.resetToDefaults(true);
        }

        //----
        $scope.resetToDefaults();
    },
    templateUrl: '/ng-post-requirement'
    }
});
