/**
 * Created by Андрей on 15.05.2015.
 */


laravel.directive('ngAttestation', function(){
    var linkFunction = function(scope, element, attributes) {
        scope.element = element;
        scope.user = scope.teacher.user;
    };

    return {
        restrict: "E",
        templateUrl: "/ng-attestation-panel",
        link: linkFunction,
        scope: {
            teacher: '=',
            post: '=',
            attestation: '='
        },
        controller: function($scope){
            $scope.remove = function(){
                $scope.$parent.removeItem($scope.attestation.id);
            }

            $scope.$on('before.attestation.removed', function(e, v){ $scope.checkAndRemove(e, v); });
            $scope.$on('attestation.safe.delete',  function(e, v){ $scope.checkAndRemove(e, v); });

            $scope.checkAndRemove = function(event, data){
                if($scope.attestation.id == data){
                    $scope.element.remove();
                }
            }
        }
    }
});
