/**
 * Created by Андрей on 25.05.2015.
 */


laravel.directive('ngRatingControl', function(){
    var linkFunction = function(scope, element, attributes) {

    };

    return {
        restrict: "E",
        templateUrl: "/ng-rating-control",
        link: linkFunction,
        scope: {
            requirement: "=req"
        },
        controller: function($scope){

        }
    }
});

