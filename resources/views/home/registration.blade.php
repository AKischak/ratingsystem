@extends('master\mainLayout')


@section('content')
<div ng-controller="registerCtrl">
    <div class="row">
        <h3>Регистрация</h3>
        <hr/>

    </div>
    <div class="row">
        <div class="col-xs-6 col-xs-offset-3">
            {!! Form::model($user, array("url"=>'registration', 'method'=>'post', 'class'=>'form')) !!}
            <div class="form-group">
                {!! Form::label('first_name', 'Имя') !!}
                {!! Form::text('first_name', $user->first_name, array('class'=>'form-control')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('last_name', 'Фамилия') !!}
                {!! Form::text('last_name', $user->last_name, array('class'=>'form-control')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('login', 'Логин') !!}
                {!! Form::text('login', $user->login, array('class'=>'form-control')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('password', 'Пароль') !!}
                {!! Form::password('password',array('class'=>'form-control')) !!}
            </div>
            <div class="form-group">
                <label for="">Желаемые роли</label>
                <div>
                    <div ng-repeat="item in roles">
                        <input type="checkbox" ng-model="item.checked" />
                        <span>@{{item.roleName}}</span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <input type="hidden" name="wishesRoles" value="@{{rolesRaw}}"/>
                {!! Form::submit("Зарегистрироваться", array('class'=>'btn btn-default')) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            @if (count($errors) > 0)
                <p class="text-danger">
                    Возникли ошибки при регистрации<br><br>
                <ul class="text-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                </p>
            @endif

        </div>
    </div>
</div>
@stop

@section("scripts")
    <script type="text/javascript">
        window.global_roles = {!!json_encode($roles)!!};
    </script>
    <script type="text/javascript" src="{{asset('/js/dev/angular/services/userRoleService.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/dev/angular/controllers/registration.js')}}"></script>
@stop