@extends('master\mainLayout')

@section('content')


    <div class="row">
        <div class="col-xs-12">
            <h3>Авторизация</h3>
            <hr/>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4 col-centered">
            {!! Form::open(array('method' => 'post')) !!}
            <div class="form-group">
                {!! Form::label("login", "Логин") !!}
                {!! Form::text("login", isset($input) ? $input['login'] : null, array('class' => 'form-control')) !!}
                @if(isset($messages) && $messages->has('login'))
                    <ul class="error-list">
                        @foreach($messages->get('login') as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>

                @endif
            </div>
            <div class="form-group">
                {!! Form::label("password","Пароль") !!}
                {!! Form::password("password", array('class' => 'form-control')) !!}
                @if(isset($messages) && $messages->has('password'))
                    <ul class="error-list">
                        @foreach($messages->get('password') as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
            <div class="form-group" style="color: red;">
                @if(isset($messages) && $messages->has('credentionals'))
                    {{$messages->first('credentionals')}}
                @endif
            </div>

            {!! Form::token() !!}
            <div>
                {!! Form::submit("Войти", array('class' => 'btn btn-primary')) !!}
                <a href="/registration" class="btn btn-default">Зарегистрироваться</a>
            </div>


            {!! Form::close() !!}
        </div>
</div>
</div>

@stop
