
@extends("master/mainLayout")

@section("content")
    <div class="row">
        <div class="col-xs-12">
            <h2>EXPERT CABINET</h2>
        </div>
        <div class="col-xs-12">
            Здравствуйте, {{$user->first_name}}

        </div>
    </div>
    <div class="row">
        <h3>1 незаконченная аттестация</h3>
        <hr/>
        <div class="col-xs-12">
            <div class="col-xs-6">
                {{$man->user->first_name}}, {{$man->user->last_name}} на должность {{$post->name}} <br/>
                <a href="/attestation" class="btn btn-default top-buffer-5">Начать аттестацию</a>
            </div>
        </div>
    </div>
@stop