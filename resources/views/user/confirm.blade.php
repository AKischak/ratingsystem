@extends('master\mainLayout')

@section('content')
<div class="row" ng-controller="confirmUserCtrl">
    <div class="col-xs-12">
        <a href="/admin" title="Перейти в админ панель" class="admin-back-link"><i class="fa fa-arrow-left fa-3x"></i></a>
        <h2>Подтверждение пользователей</h2>
        <hr/>
    </div>
    <div class="col-xs-12">
        <div style="margin-top: 15px; margin-bottom: 15px;">
            <h4>
                @if($users && count($users) > 0)
                    Неподтвержденных пользователей {{count($users)}}
                @else
                    Нет неподтвержденных пользователей!
                @endif
            </h4>
        </div>
    </div>
    @if($users && count($users) > 0)
    <div class="col-xs-12">
        <table class="table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Имя</th>
                <th>Логин</th>
                <th>Дата регистрации</th>
                <th>Роли</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->first_name}} {{$user->last_name}}</td>
                    <td>{{$user->login}}</td>
                    <td class="col-xs-2">{{$user->created_at}}</td>
                    <td class="col-xs-6">
                        <div class="col-xs-5">
                            <div ng-repeat="item in allWishes[{{$user->id}}]">
                                <input type="checkbox" id="check-{{$user->id}}-@{{ item.id }}" ng-model="item.checked" ng-disabled="userStatus[{{$user->id}}] != 1"/>
                                <label for="check-{{$user->id}}-@{{ item.id }}">@{{ item.name }}</label>
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <button class="btn btn-default" ng-click="saveRoles({{$user->id}})" ng-show="userStatus[{{$user->id}}] == 1">Подтвердить</button>
                            <img class="icon-small" src="/images/preloader.gif" ng-show="userStatus[{{$user->id}}] == 2" alt=""/>
                            <img class="icon-small" src="/images/success.png" ng-show="userStatus[{{$user->id}}] == 3" alt=""/>
                            <span ng-show="userStatus[{{$user->id}}] == 3">Пользователь подтвержден</span>
                        </div>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @endif

</div>
@stop

@section("scripts")
    <script type="text/javascript">
        window.allWishes = {!! json_encode($allWishes) !!};
        window.allRoles = {!! json_encode($allRoles) !!};
    </script>
    <script type="text/javascript" src="{{asset('/js/dev/angular/services/userRoleService.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/dev/angular/controllers/userConfirm.js')}}"></script>
@stop