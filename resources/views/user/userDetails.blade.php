@extends('master\mainLayout')

@section('content')
    <div ng-controller="userCtrl">
        <h3>Информация о пользователе</h3>
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-4">
                    <label>Name</label>
                </div>
                <div class="col-xs-8">
                    {{$model->first_name}}, {{$model->last_name}}
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <label>Login</label>
                </div>
                <div class="col-xs-8">
                    {{$model->login}}
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <label>Roles</label>
                </div>
                <div class="col-xs-12">
                    <div ng-repeat="item in userRoles track by $index" class="row">
                        <div class="col-xs-1">
                            <input type="checkbox" class="checkbox" style="width: 15px; height: 15px;" ng-model="item.isSelected"/>
                        </div>
                        <div class="col-xs-11">
                            <label>@{{item.name}}</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button ng-click="saveRoles()" ng-disabled="status != 1" class="btn btn-primary">Сохранить роли</button>
                    <img class="icon-small" src="/images/preloader.gif" ng-show="status == 2" alt=""/>
                    <img class="icon-small" src="/images/success.png" ng-show="status == 3" alt=""/>
                </div>
            </div>
        </div>
    </div>
@stop



@section("scripts")
    <script type="text/javascript">
        window.global_roles = {!!json_encode($roles)!!};
        window.user_roles = {!!json_encode($model->roles)!!};
        window.user = {!!json_encode($model)!!};
    </script>
    <script type="text/javascript" src="{{asset('/js/dev/angular/services/userRoleService.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/dev/angular/controllers/users.js')}}"></script>
@stop