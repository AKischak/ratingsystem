@extends('master\mainLayout')

@section('content')
    <div ng-controller="userCtrl">
        <div class="col-xs-12">
            <div class="col-xs-12">
                <a href="/admin" title="Перейти в админ панель" class="admin-back-link"><i class="fa fa-arrow-left fa-3x"></i></a>
                <h2>Пользователи</h2>
                <hr/>
            </div>

            <div class="col-xs-12">
                @if($users && count($users) > 0)
                    Пользователей: {{count($users)}}
                @else
                    В системе отсутствуют пользователи!
                @endif
            </div>
            <div class="col-xs-6">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Логин</th>
                        <th>Редактировать</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $value)
                        <tr>
                            <td>{{$value->id}}</td>
                            <td>{{$value->first_name}}, {{$value->last_name}}</td>
                            <td>
                                <a href="/user-edit/{{$value->id}}">Изменить</a> |
                                <a href="/user-remove/{{$value->id}}">Удалить</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
@stop



