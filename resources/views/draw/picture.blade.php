@extends("master/cleanLayout")


@section("content")

<div class="canvas-wrapper" id="canvas-wrap">
    <canvas id="draw-field" height="234" width="234"></canvas>
</div>
@stop


@section("scripts")
    <script type="text/javascript" src="{{asset('/js/Dev/draw.js')}}"></script>
@stop