
@extends("master/mainLayout")

@section("content")
    <div ng-controller="attRatingCtrl">
        <div class="row">
            <div class="col-xs-12">
                <h2>Аттестация</h2>
            </div>
            <div class="col-xs-12">
                Аттестация {{$man->user->first_name." ".$man->user->last_name}} на должность {{$post->name}}

            </div>
        </div>
        <div class="row"  style="padding-top: 10px; padding-bottom: 10px; background-color: rgba(18, 20, 29, 0.73);">
            <div class="col-xs-6 col-xs-offset-3">
                <div class="form">
                    <div class="col-xs-12" id="controls-wrapper">
                        <div ng-repeat="item in requirements">
                            <ng-rating-control req="item"></ng-rating-control>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <a href="/attestation-results" class="btn btn-success top-buffer-20 center-block">Закончить</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section("scripts")
    <script type="text/javascript">
        window.req = {!! json_encode($post->Requirements)!!};
    </script>
    <script type="text/javascript" src="{{asset('/js/dev/angular/services/expertGroupService.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/dev/angular/directives/ratingControl.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/dev/angular/controllers/attRating.js')}}"></script>
@stop