@extends("master/mainLayout")

@section("content")
    <div class="row">
        <div class="col-xs-12">
            <h3>Пройденные аттестации</h3>
            <hr/>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-striped">
                <thead class="text-center">
                    <tr>
                        <th class="text-center">Сотрудник</th>
                        <th class="text-center">Должность</th>
                        <th class="text-center">Набраный бал</th>
                        <th class="text-center">Проходное значене</th>
                        <th class="text-center">Результат</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    <tr>
                        <td>{{$man->user->first_name}} {{$man->user->last_name}}</td>
                        <td>{{$post->name}}</td>
                        <td>8.4 из 10</td>
                        <td>6</td>
                        <td>Соответствует, с некоторыми замечаниями</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop
