@extends("master.mainLayout")


@section("content")
    <div class="row">
        <div class="col-xs-12">
            <h2>
                Система рейтингового контроля специалистов
            </h2>
            Административная часть
            <hr/>
        </div>
        <div class="col-xs-12">
            <a href="/expert-group-list" class="btn btn-success">Управление экспертными группами</a>
            <a href="/post-list" class="btn btn-primary">Управление должностями</a>
            <a href="/users" class="btn btn-danger">Управление пользователями</a>
            <a href="/confirm-users" class="btn btn-warning">Подтверждение пользователей</a>
        </div>
    </div>

@stop