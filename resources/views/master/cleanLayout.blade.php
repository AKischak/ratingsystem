<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Laravel</title>
    <link rel="stylesheet" href="{{ asset('/css/bootstrap/bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/css/FontAwesome/css/font-awesome.min.css') }}"/>
    <link href='http://fonts.googleapis.com/css?family=Exo:300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href="{{asset('/css/laravel_main.css') }}" rel="stylesheet">
</head>
<body>
<div>
    @yield('content')
</div>
<script type="text/javascript" src="{{asset('/js/jquery-2.1.3.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/bootstrap/bootstrap.min.js')}}"></script>
@yield("scripts")
</body>
</html>