<!DOCTYPE html>
<html lang="en" ng-app="laravel">
<head>
    <meta charset="UTF-8">
    <title>Rating system</title>
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap/bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/css/FontAwesome/css/font-awesome.min.css') }}"/>
    <link href='http://fonts.googleapis.com/css?family=Exo:300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href="{{asset('/css/helpers.css') }}" rel="stylesheet">
    <link href="{{asset('/css/laravel_main.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('/css/movingBackground.css')}}"/>
    @yield("styles")
</head>
<body>
<nav class="navbar navbar-inverse navbar-static-top" id="main-navbar">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <img src="/images/flag.gif" alt="" id="logo-gif"/>
            <a class="navbar-brand" href="/">
                {{env('PROJECT_NAME')}}
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="/">Home</a></li>
                <li class="{{ Request::is( 'about') ? 'active' : '' }}"><a href="/about">About</a></li>
                <li class="{{ Request::is( 'contact') ? 'active' : '' }}"><a href="/contact">Contact</a></li>
                @if(!Auth::guest())
                @if(Auth::user()->HasPermission(Role::ADMIN))
                <li class="{{ Request::is( 'admin') ? 'active' : '' }}"><a href="/admin">Админ панель</a></li>
                @endif
                @if(Auth::user()->HasPermission(Role::TEACHER))
                <li class="{{ Request::is( 'teacher-cabinet') ? 'active' : '' }}"><a href="/teacher-cabinet">Кабинет сотрудника</a></li>
                @endif
                @if(Auth::user()->HasPermission(Role::EXPERT))
                <li class="{{ Request::is( 'expert-cabinet') ? 'active' : '' }}"><a href="/expert-cabinet">Кабинет эксперта</a></li>
                @endif
                @endif
            </ul>
        </div>
        <div class="user-region">
            @if(!Request::is('login') && Auth::guest())
            <a href="/login" id="login-link" title="Авторизация"><i class="fa fa fa-sign-in fa-2x"></i></a>
            @elseif(Auth::check())
            <div class="user-welcome">
                <span class="user-welcome-text">Здравствуйте,  {{ Auth::user()->first_name }}</span>
                <a href="/logout" class="logout-button"><i class="fa fa fa-sign-out fa-2x"></i></a>
            </div>
            @endif
        </div>
    </div>
</nav>

<div class="root-layout-wrapper">
    @yield('root-content')
</div>

</body>
</html>