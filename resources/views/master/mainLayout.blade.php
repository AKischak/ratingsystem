@extends("master.rootLayout")

@section("root-content")
<div class="container" id="main-container">
	@yield('content')
</div>

<script type="text/javascript" src="{{asset('/js/jquery-2.1.3.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/angular/angular.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/underscore-min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/dev/angular/app.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/bootstrap/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/Dev/backgroundMoving.js')}}"></script>
@yield("scripts")

@stop