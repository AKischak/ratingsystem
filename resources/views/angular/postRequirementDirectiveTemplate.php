<div class="row">
    <div class="col-xs-12">
        <div class="post-requirement-block panel panel-default">
            <div class="panel-body">
                <div class="col-xs-12">
                    <h4>
                        <span ng-show="editType == 1">Создать новое требование</span>
                        <span ng-show="editType == 2">Изменить требование: {{requirementName}}</span>
                    </h4>
                </div>
                <form class="form-horizontal" name="postReqForm">
                    <div class="form-group" ng-class="{ 'has-error' : postReqForm.reqName.$invalid}">
                        <label for="" class="col-xs-4 control-label">Название <span class="red">*</span></label>

                        <div class="col-xs-8" style="position:relative;">
                            <input type="text" class="form-control top-buffer-5 bg-danger" name='reqName'
                                   required="required" placeholder="Название" ng-model="requirement.name"/>
                        </div>
                    </div>
                    <div class="form-group" ng-class="{ 'has-error' : postReqForm.minValue.$invalid}">
                        <label for="" class="col-xs-4 control-label">Минимальное значение</label>

                        <div class="col-xs-8">
                            <input type="number" class="form-control top-buffer-5"
                                   name="minValue" placeholder="Минимальное значение" ng-model="requirement.min"/>
                            <span ng-if="postReqForm.minValue.$error.Custom" class="error-text">
                            Значение должно быть больше 0 и меньше максимального
                        </span>
                        </div>
                    </div>
                    <div class="form-group" ng-class="{ 'has-error' : postReqForm.maxValue.$invalid}">
                        <label for="" class="col-xs-4 control-label">Максимальное значение</label>

                        <div class="col-xs-8">
                            <input type="number" class="form-control top-buffer-5" name="maxValue"
                                   placeholder="Максимальное значение" ng-model="requirement.max"/>
                            <span ng-if="postReqForm.maxValue.$error.Custom" class="error-text">
                            Значение должно быть больше минимального
                        </span>
                        </div>
                    </div>
                    <div class="form-group" ng-show="requirement.valueType == 'number' || !requirement.valueType" ng-class="{ 'has-error' : postReqForm.thresholdValue.$invalid}">
                        <label for="" class="col-xs-4 control-label">Пороговое значение</label>

                        <div class="col-xs-8">
                            <input type="number" class="form-control top-buffer-5" name="thresholdValue" step="0.1"
                                   placeholder="Максимальное значение" ng-model="requirement.threshold"/>
                        <span ng-if="postReqForm.thresholdValue.$error.Interval" class="error-text">
                            Пороговое значение должно быть в интервале минимального и максимального
                        </span>
                        </div>
                    </div>
                    <div class="form-group" ng-class="{ 'has-error' : postReqForm.valueType.$invalid}">
                        <label for="" class="col-xs-4 control-label">Тип значения</label>
                        <div class="col-xs-8">
                            <div class="top-buffer-5">
                                <input id="asdf4qfsdf" type="radio" ng-model="requirement.valueType" value="number"/>
                                <label for="asdf4qfsdf">Числовой</label>
                                <input id="43kq5jbrnp" type="radio" ng-model="requirement.valueType" value="bool"/>
                                <label for="43kq5jbrnp">Логический</label>
                            </div>
                        </div>
                    </div>
                    <div style="text-align: center;">
                        <a href="#" class="btn btn-success top-buffer-10" ng-disabled="postReqForm.$invalid"
                                ng-click="processData(postReqForm)" style="width: 100px;">
                            {{ editType == 1 ? 'Добавить' : 'Сохранить'}}
                        </a>
                        <a href="#" ng-show="editType == 2" class="btn btn-danger top-buffer-10" style="width: 100px;" ng-click="abortEdit()">Отмена</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
