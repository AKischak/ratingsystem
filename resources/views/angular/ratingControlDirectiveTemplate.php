<div class="form top-buffer-5 col-xs-12">
    <div class="form-group">
        <label for="" class="col-xs-4">{{requirement.name}}</label>
        <input class="col-xs-6" style="width: 200px;" type="range" step="1" min="{{requirement.min}}" max="{{requirement.max}}" ng-model="requirement.value"/>
        <div class="col-xs-1">
            <img class="icon-small" src="/images/zapret.png" ng-show="requirement.value - requirement.threshold < 0" alt=""/>
            <img class="icon-small" src="/images/success_green.png" ng-show="requirement.value - requirement.threshold >= 0" alt=""/>
        </div>
        <div class="col-xs-1 text-right">
            {{requirement.value}}
        </div>
    </div>
    <div class="col-xs-12">
        <hr/>
    </div>
</div>
