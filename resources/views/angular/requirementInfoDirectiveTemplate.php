<div class="post-requirement-info panel">
    <div class="panel-body" ng-class="(requirement.isNew || requirement.isUpdated)  ? 'bg-danger' : 'bg-success'">
        <div class="row">
            <div class="col-xs-12">
                <label class="form-label">{{requirement.name}}</label>
                {{requirement.valueType == "number" ? "(Числовой)" : (requirement.valueType == "bool" ? "(Логический)" : '') }}
            </div>
            <div class="col-md-4">
                Минимальная оценка: {{requirement.min}}
            </div>
            <div class="col-md-4">
                Максимальная оценка: {{requirement.max}}
            </div>
            <div class="col-md-2">
                Порог: {{requirement.threshold}}
            </div>
            <div class="col-md-2">
                <div class="col-xs-6">
                    <a href="#" ng-click="edit()" title="Изменить"><i class="fa fa-pencil-square-o fa-2x"></i></a>
                </div>
                <div class="col-xs-6">
                    <a href="#" ng-click="remove()" style="color: red;" title="Удалить"><i class="fa fa-times fa-2x"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>