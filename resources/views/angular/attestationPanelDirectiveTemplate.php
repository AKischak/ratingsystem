<div class="attestation-panel panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Аттестация №{{attestation.isNew ? 'Не сохранено!' : attestation.id}}</h3>
    </div>
    <div class="panel-body" ng-class="attestation.isNew ? 'bg-red': 'bg-primary'">
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-4">Аттестуемый</div>
                <div class="col-xs-8">
                    <a class="attestation-name" href="/user-edit/{{user.id}}" target="_blank">
                        {{user.first_name}} {{user.last_name}}
                    </a>
                </div>
            </div>
            <div class="col-xs-12">
                <hr/>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-4">Должность</div>
                <div class="col-xs-8">{{post.name}}</div>
            </div>
        </div>
        <div><a href="#" class="remove-btn" ng-click="remove()"><i class="fa fa-times"></i></a></div>
    </div>
</div>