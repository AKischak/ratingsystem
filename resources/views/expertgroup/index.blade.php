@extends("master/mainLayout")

@section("content")
    <div class="row">
        <div class="col-xs-12">
            <a href="/admin" title="Перейти в админ панель" class="admin-back-link"><i class="fa fa-arrow-left fa-3x"></i></a> <h2>Экспертные группы</h2>
            <hr/>
        </div>
        <div class="col-xs-12">
            <a href="/create-expert-group" class="btn btn-primary">Добавить новую группу</a>
            <table class="table">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Название</th>
                    <th>Метка</th>
                    <th>Редактировать</th>
                </tr>
                </thead>
                <tbody>
                @foreach($groups as $group)
                    <tr>
                        <td>{{$group->id}}</td>
                        <td>{{$group->name}}</td>
                        <td>
                            <p style="max-width: 300px;  max-height: 200px;  overflow: hidden;  white-space: nowrap;  text-overflow: ellipsis;">
                                {{$group->label}}
                            </p>
                        </td>
                        <td>
                            <a href="/update-expert-group/{{$group->id}}">Изменить</a> |
                            <a href="/delete-expert-group/{{$group->id}}">Удалить</a> |
                            <a href="/modify-experts/{{$group->id}}">Состав</a> |
                            <a href="/manage-attestation/{{$group->id}}">Атестации</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop