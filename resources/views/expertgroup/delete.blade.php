@extends("master/mainLayout")

@section("content")
<div class="row">
    <div class="col-xs-12">
        <h4>Удаление группы</h4>
        <hr/>
    </div>
    <div class="col-xs-12">
        {!! Form::model($group, array("url"=>'delete-expert-group', 'method'=>'post', 'class'=>'form')) !!}
            Действидельно удалить группу {{$group->name}}?
        {!! Form::hidden('id', $group->id) !!}
        {!! Form::submit('Удалить', array('class' => 'btn btn-danger'))!!}
        {!! Form::close() !!}
    </div>
</div>
@stop