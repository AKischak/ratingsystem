@extends("master/mainLayout")

@section("content")
    <?php $postUrl = 'update-expert-group'; ?>
    <div class="row" ng-controller="expertGroupCtrl">
        <div class="col-xs-1">
            <a href="/expert-group-list" title="Назад" class="admin-back-link"><i class="fa fa-arrow-left fa-3x"></i></a>
        </div>
        <div class="col-xs-11">
            <h3>Обновление состава экспертной группы </h3>
            <h4>{{$group->name}}</h4>
            <hr/>
        </div>
        <div class="col-xs-12">
            <div class="col-md-2">
                Состав группы: @{{message}}
            </div>
            <div class="col-md-6">
            <div id="users-wrapper">

            </div>
            </div>
            <div class="col-md-4">
                <div class="row" style="padding-top: 5px;">
                    <div class="col-xs-8">
                        <div class="dropdown dropdown-custom">
                            <button class="btn btn-default dropdown-toggle" type="button" ng-disabled="expertsToAdd.length == 0" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true" style="width: 200px;">
                                @{{ selectedExpert.name || "-" }}
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li ng-repeat="item in expertsToAdd">
                                    <a role="menuitem" tabindex="-1" ng-click="selectExpert(item.id)">@{{ item.name }}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <button ng-click="addExpert()" ng-disabled="expertsToAdd.length == 0" class="btn btn-primary">+</button>
                    </div>
                    <div class="col-xs-12" style="padding-top: 10px;">
                        <button class="btn btn-primary" ng-click="saveExperts()" ng-show="isChanged" ng-disabled="saveStatus != 1">Сохранить</button>
                        <img class="icon-small" src="/images/preloader.gif" ng-show="saveStatus == 2" alt=""/>
                        <img class="icon-small" src="/images/success.png" ng-show="saveStatus == 3" alt=""/>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section("scripts")
    <script type="text/javascript">
        window.group = {!! json_encode($group) !!};
        window.groupExperts = {!! json_encode($groupExperts) !!};
        window.allExperts = {!! json_encode($allExperts) !!};
    </script>
    <script type="text/javascript" src="{{asset('/js/dev/angular/services/expertGroupService.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/dev/angular/directives/expertPanel.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/dev/angular/controllers/expertGroup.js')}}"></script>
@stop