@extends("master/mainLayout")

@section("content")
<?php $postUrl = 'update-expert-group'; ?>
<div class="row">
    <div class="col-xs-12">
        <h4>Обновление экспертной группы {{$group->name}}</h4>
        <hr/>
    </div>
    <div class="col-xs-12">
        @include('expertgroup.formPartial')
    </div>
</div>
@stop