@extends("master/mainLayout")

@section("content")
    <div class="row" ng-controller="attestationCtrl">
        <div class="col-xs-1">
            <a href="/expert-group-list" title="Назад" class="admin-back-link"><i class="fa fa-arrow-left fa-3x"></i></a>
        </div>
        <div class="col-xs-11">
            <h3>Обновление списка аттестаций </h3>
            <h4>{{$group->name}}</h4>
            <hr/>
        </div>
        <div class="col-xs-12">
            <div class="col-md-6">
                <h3>Группа аттестует</h3>
                <hr/>
                <div id="users-wrapper">
                </div>
            </div>
            <div class="col-md-6">
                <h4>Добавить новую аттестацию</h4>
                <div class="row" style="padding-top: 5px;">
                    <div class="col-xs-12 top-buffer-10">
                        <label for="selectTeacherDropdown" class="col-md-4">Преподаватель</label>
                        <div class="col-md-8">
                            <div class="dropdown dropdown-custom">
                                <button class="btn btn-default dropdown-toggle"
                                        type="button" ng-disabled="teachersToAdd.length == 0"
                                        id="selectTeacherDropdown" data-toggle="dropdown" aria-expanded="true">
                                    @{{ selectedTeacher.name || "-" }}
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="selectTeacherDropdown">
                                    <li ng-repeat="item in teachersToAdd">
                                        <a role="menuitem" tabindex="-1" class="break-text" ng-click="selectTeacher(item.id)">@{{ item.name }}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 top-buffer-10">
                        <label for="selectPostDropdown" class="col-md-4">Профессия</label>
                        <div class="col-md-8">
                            <div class="dropdown dropdown-custom">
                                <button class="btn btn-default dropdown-toggle"
                                        type="button" ng-disabled="postsToAdd.length == 0"
                                        id="selectPostDropdown" data-toggle="dropdown" aria-expanded="true">
                                    @{{ selectedPost.name ? (selectedPost.name.length > 30 ? selectedPost.name.substring(0, 30)+"..." : selectedPost.name)  : "-" }}
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="selectPostDropdown">
                                    <li ng-repeat="item in postsToAdd">
                                        <a role="menuitem" tabindex="-1" class="break-text" title="@{{item.name}}" ng-click="selectPost(item.id)">@{{ item.name }}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 top-buffer-10">
                        <button ng-click="addAttestation()" ng-disabled="!canAdd" class="btn btn-primary center-block">Добавить</button>
                    </div>
                    <div class="col-xs-12" style="padding-top: 10px;">
                        <hr/>
                        <button class="btn btn-success center-block" ng-click="saveAttestation()" ng-show="isChanged" ng-disabled="saveStatus != 1">Сохранить</button>
                        <img class="icon-small" src="/images/preloader.gif" ng-show="saveStatus == 2" alt=""/>
                        <img class="icon-small" src="/images/success.png" ng-show="saveStatus == 3" alt=""/>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section("scripts")
    <script type="text/javascript">
        window.group = {!! json_encode($group) !!};
        window.attestations = {!! json_encode($attestations) !!};
        window.allTeachers  = {!! json_encode($allTeachers) !!};
        window.posts  = {!! json_encode($posts) !!};
    </script>
    <script type="text/javascript" src="{{asset('/js/dev/angular/services/expertGroupService.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/dev/angular/directives/attestationPanel.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/dev/angular/controllers/attestation.js')}}"></script>
@stop