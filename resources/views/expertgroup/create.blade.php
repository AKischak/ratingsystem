@extends("master/mainLayout")

@section("content")
<?php $postUrl = 'create-expert-group'; ?>
<div class="row">
    <div class="col-xs-12">
        <h4>Создание экспертной группы</h4>
        <hr/>
    </div>
    <div class="col-xs-12">
        @include('expertgroup.formPartial')
    </div>
</div>
@stop