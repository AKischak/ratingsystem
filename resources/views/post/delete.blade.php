@extends("master/mainLayout")

@section("content")
<div class="row">
    <div class="col-xs-12">
        <h4>Удаление должности</h4>
        <hr/>
    </div>
    <div class="col-xs-12">
        {!! Form::model($post, array("url"=>'delete-post', 'method'=>'post', 'class'=>'form')) !!}
            Действидельно удалить долєность "{{$post->name}}"?
        {!! Form::hidden('id', $post->id) !!}
        {!! Form::submit('Удалить', array('class' => 'btn btn-danger'))!!}
        {!! Form::close() !!}
    </div>
</div>
@stop