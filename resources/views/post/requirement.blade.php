@extends("master/mainLayout")

@section("content")
<div class="row" ng-controller="postRequirementCtrl">
    <div class="col-xs-12">
        <div class="col-xs-12">
            <a href="/post-list" title="Список должностей" class="admin-back-link">
                <i class="fa fa-arrow-left fa-3x" ></i>
            </a>
            <h2>Требования к должности</h2>
            <hr/>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="col-xs-2">
            <h3>{{$post->name}}</h3>
            <button class="btn btn-primary" ng-click="SaveToServer()" ng-show="isChanged" ng-disabled="saveStatus != 1">Сохранить</button>
            <img class="icon-small" src="/images/preloader.gif" ng-show="saveStatus == 2" alt=""/>
            <img class="icon-small" src="/images/success.png" ng-show="saveStatus == 3" alt=""/>
        </div>
        <div class="col-xs-8">
            <div class="col-xs-12">
                <h4 ng-show="storageRequirements.length == 0">Список требований пуст</h4>
                <h4 ng-show="storageRequirements.length > 0">Список требований:</h4>
                <hr/>
            </div>
            <div class="col-xs-12">
                <div id="requirements-container">

                </div>
                <ng-post-requirement></ng-post-requirement>
            </div>
        </div>
        <div class="col-xs-2">
        </div>
    </div>
</div>
@stop

@section("scripts")
    <script type="text/javascript">
        window.requirements = {!! json_encode($post->Requirements) !!};
        window.postId = {!!$post->id!!};
    </script>
    <script type="text/javascript" src="{{asset('/js/dev/angular/services/postService.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/dev/angular/directives/postRequirementFormDirective.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/dev/angular/directives/requirementInfoDirective.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/dev/angular/controllers/postRequirement.js')}}"></script>
@stop