@extends("master/mainLayout")

@section("content")
<?php $postUrl = 'create-post'; ?>
<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-12">
            <a href="/post-list" title="Список должностей" class="admin-back-link">
                <i class="fa fa-arrow-left fa-3x" ></i>
            </a>
            <h2>Создание новой должности</h2>
            <hr/>
        </div>
    </div>
    <div class="col-xs-12">
        @include('post.formPartial')
    </div>
</div>
@stop