

<div class="row">
    <div class="col-xs-6 col-xs-offset-3">
        {!! Form::model($post, array("url"=>$postUrl, 'method'=>'post', 'class'=>'form')) !!}
        <div class="form-group">
            {!! Form::label('name', 'Название') !!}
            {!! Form::text('name', $post->name, array('class'=>'form-control')) !!}
        </div>

        <div class="form-group">
            {!! Form::label('label', 'Описание') !!}
            {!! Form::textArea('label', $post->label, array('class'=>'form-control')) !!}
        </div>

        <div class="form-group">
            {!! Form::token() !!}
            {!! Form::hidden("id", isset($post->id) ? $post->id : 0) !!}
            {!! Form::submit("Сохранить", array('class'=>'btn btn-primary')) !!}
        </div>
        {!! Form::close() !!}
    </div>
</div>