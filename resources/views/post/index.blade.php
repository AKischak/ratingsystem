
@extends("master/mainLayout")

@section("content")
    <div class="row">
        <div class="col-xs-12">
            <a href="/admin" title="Перейти в админ панель" class="admin-back-link"><i class="fa fa-arrow-left fa-3x"></i></a>
            <h2>Должности для атестации</h2>
            <hr/>
        </div>
        <div class="col-xs-12">
            <a href="/create-post" class="btn btn-primary">Добавить новую должность</a>
            <table class="table">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Название</th>
                    <th>Редактировать</th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)
                    <tr>
                        <td>{{$post->id}}</td>
                        <td>{{$post->name}}</td>
                        <td>
                            <a href="/update-post/{{$post->id}}">Изменить</a> |
                            <a href="/delete-post/{{$post->id}}">Удалить</a> |
                            <a href="/post-req/{{$post->id}}">Требования</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop