<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expert extends Model {

    protected $table = "experts";


    public function Groups(){
        return $this->belongsToMany('App\Models\ExpertGroup', 'expert_groups', 'expert_id', 'group_id');
    }

    public function User(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }

}
