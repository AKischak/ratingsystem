<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpertGroup extends Model {

    protected $table = "examination_group";

    protected $guarder = array("id");

    protected $fillable = array('name', 'label');

    public function teachers(){
        return $this->belongsToMany('App\Models\Teacher', "attestations", 'group_id', "teacher_id");
    }

    public function Attestations(){
        return $this->hasMany('App\Models\Attestation', 'group_id');
    }



    public function experts(){
        return $this->belongsToMany('App\Models\Expert', "expert_groups", 'group_id', "expert_id");
    }
}
