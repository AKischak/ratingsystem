<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * @property mixed password
 * @property mixed email
 * @property mixed login
 */

class User extends Model implements AuthenticatableContract, CanResetPasswordContract{

    use Authenticatable, CanResetPassword;

	protected $table = "users";


    protected $guarder = array("id");

    protected $fillable = array('first_name', 'last_name', 'login', 'password');

    public function roles(){
        return $this->belongsToMany('App\Models\Role', 'user_roles');
    }

    public function HasPermission($roleId){
        return $this->roles->contains($roleId);
    }

    public function RegistrationWishes(){
        return $this->hasOne('App\Models\RegistrationWishes');
    }

    public function Expert(){
        return $this->hasOne('App\Models\Expert');
    }

    public function Teacher(){
        return $this->hasOne('App\Models\Teacher');
    }

}
