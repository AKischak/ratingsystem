<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attestation extends Model {

	protected $table = 'attestations';


    public function Teacher(){
        return $this->belongsTo('App\Models\Teacher', 'teachers', 'teacher_id');
    }

    public function ExpertGroup(){
        return $this->belongsTo('App\Models\ExpertGroup', 'teachers', 'group_id');
    }

    public function Post(){
        return $this->belongsTo('App\Models\Post', 'posts', 'post_id');
    }

    public function Results(){
        return $this->hasMany('App\Models\AttestationResult', 'attestation_id', 'attestation_results');
    }
}
