<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostRequirement extends Model {



	protected $table = "post_requirements";

    public function FillFromObject($data){
        $this->min_value = $data->min;
        $this->max_value = $data->max;
        $this->threshold = $data->threshold;
        $this->value_type = $data->valueType;
        $this->name = $data->name;
    }

}
