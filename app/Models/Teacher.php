<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model {

    protected $table = "teachers";


    public function ExaminatorGroups(){
        return $this->belongsToMany('App\Models\ExpertGroup', 'teacher_groups');
    }

    public function User(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
