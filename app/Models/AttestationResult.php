<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttestationResult extends Model {

    protected $table = 'attestation_results';

}
