<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttestationResultItem extends Model {

    protected $table = 'attestation_result_items';

}
