<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

    protected $table = "posts";

    protected $guarder = array("id");

    protected $fillable = array('name', 'label');

    public function Requirements(){
        return $this->hasMany('App\Models\PostRequirement', 'post_id');
    }

}
