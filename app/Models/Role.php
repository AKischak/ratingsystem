<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

    const ADMIN = 1;
    const USER = 2;
    const TEACHER = 3;
    const EXPERT = 4;
    const GUEST = 0;


	protected $table = "roles";

    public function users(){
        return $this->belongsToMany('User', 'user_roles');
    }

}
