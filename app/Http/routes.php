<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'SinglePageController@index');
Route::get('about', 'HomeController@about');
Route::get('contact', 'HomeController@contact');

Route::get('test', function(){
    $f = function(){
        /*$result = new stdClass;
        $result->error = true;
        $result->errorMsg = "Exception!";*/
        $result = array(
            'error'=>true
            );

        return $result;
    };
    $res = $f();
    dd($res);
});

Route::any('db/{action?}', 'SinglePageController@DataBaseTesting');



Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('login', 'HomeController@login');
Route::post('login', 'HomeController@loginPost');
Route::any('logout', 'HomeController@logout');

Route::get('registration', 'HomeController@Register');
Route::post('registration', 'HomeController@RegisterPost');

Route::get("draw/picture", "DrawingController@Picture");

Route::get("secret", array('middleware' => 'auth', 'uses' => "SecretController@index"));

Route::filter('accessVerify', function ($route, $request, $value) {
    if(!Auth::guest() && !Auth::user()->HasPermission($value)){
        return Redirect::to('/');
    }
});

Route::group(array('middleware' => 'auth'), function() {
    Route::get("teacher-cabinet", array('before' => "accessVerify:" . Role::TEACHER, 'uses' => "TeacherController@Cabinet"));
    Route::get("expert-cabinet", array('before' => "accessVerify:" . Role::EXPERT, 'uses' => "ExpertController@Cabinet"));
    Route::get("attestation", array('before' => "accessVerify:" . Role::EXPERT, 'uses' => "AttestationController@index"));
    Route::get("attestation-results", "AttestationController@Results");
});


Route::get("401", "AdminController@Error401");

Route::group(array('middleware' => 'admin'), function(){
    // --- ПОЛЬЗОВАТЕЛИ
    Route::get("admin", "AdminController@Index");
    Route::get("users", "UserController@UserManager");
    Route::get("user-edit/{id}", "UserController@UserDetail");
    Route::get("user-remove/{id}", "UserController@UserRemove");
    Route::post("userRoleUpdate", "UserController@UserRoleUpdate");
    Route::get("confirm-users", "UserController@NewUsers");
    Route::post("tryConfirm", "UserController@TryConfirmUser");

    // --- ЭКСПЕРТНЫЕ ГРУППЫ
    Route::get('expert-group-list', "ExpertGroupController@index");
    Route::get('create-expert-group', "ExpertGroupController@create");
    Route::post('create-expert-group', array('before' => "csrf", 'uses' => "ExpertGroupController@createPost"));
    Route::get('update-expert-group/{id}', "ExpertGroupController@update");
    Route::post('update-expert-group', array('before' => "csrf", 'uses' => "ExpertGroupController@updatePost"));
    Route::get('delete-expert-group/{id}', "ExpertGroupController@delete");
    Route::post('delete-expert-group', array('before' => "csrf", 'uses' => "ExpertGroupController@deletePost"));

    // -- СОСТАВ ЭКСПЕРТНЫХ ГРУПП
    Route::get('modify-experts/{id}', "ExpertGroupController@ExpertModify");
    Route::post('save-group-experts', "ExpertGroupController@SaveExperts");

    // -- АТЕСТАЦИИ
    Route::get('manage-attestation/{id}', "ExpertGroupController@TeacherAttestation");
    Route::post('save-attestation', "ExpertGroupController@SaveAttestation");

    // -- ДОЛЖНОСТИ(ПРОФЕССИИ)
    Route::get('post-list', "PostController@index");
    Route::get('create-post', "PostController@create");
    Route::post('create-post', array('before' => "csrf", 'uses' => "PostController@createPost"));
    Route::get('update-post/{id}', "PostController@update");
    Route::post('update-post', array('before' => "csrf", 'uses' => "PostController@updatePost"));
    Route::get('delete-post/{id}', "PostController@delete");
    Route::post('delete-post', array('before' => "csrf", 'uses' => "PostController@deletePost"));

    Route::get('post-req/{id}', "PostController@PostRequirements");
    Route::post('post-req-update', "PostController@UpdateRequirements");

});


//Route::post('loginConfirm', 'HomeController@loginConfirm');


//========== ANGULAR DIRECTIVE TEMPLATES ============

Route::any("ng-user-block", function(){
    return view('angular.userBlockDirectiveTemplate');
});
Route::any("ng-attestation-panel", function(){
    return view('angular.attestationPanelDirectiveTemplate');
});
Route::any("ng-post-requirement", function(){
    return view('angular.postRequirementDirectiveTemplate');
});
Route::any("ng-requirement-info", function(){
    return view('angular.requirementInfoDirectiveTemplate');
});
Route::any("ng-rating-control", function(){
    return view('angular.ratingControlDirectiveTemplate');
});