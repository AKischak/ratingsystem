<?php namespace App\Http\Middleware;

use App\Models\Role;
use Closure;
use Illuminate\Contracts\Auth\Guard;

class AdminAccess {

    protected $auth;


    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }


	public function handle($request, Closure $next)
	{
        if ($this->auth->guest() || !$this->auth->user()->HasPermission(Role::ADMIN))
        {
            if ($request->ajax())
            {
                return response('Unauthorized.', 401);
            }
            else
            {
                return redirect("401");
            }
        }
		return $next($request);
	}

}
