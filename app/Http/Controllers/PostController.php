<?php namespace App\Http\Controllers;

use App\Http\Requests;

use App\Models\Expert;
use App\Models\ExpertGroup;
use App\Models\Post;
use App\Models\PostRequirement;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller {

	public function index()
	{
		$posts = Post::all();

        return view('post.index', compact('posts'));
	}

	public function create()
	{
        $post = new Post();
        return view('post.create', compact('post'));
	}

    public function createPost()
    {
        $post = new Post;
        $validator = $this->store($post);
        if($validator){
            return view('post.create', compact('post'))->withErrors($validator);
        }
        return redirect('post-list');
    }

    public function update($id){
        $post = Post::find($id);
        return view('post.update', compact('post'));
    }
    public function updatePost()
    {
        $post = Post::find(Input::get('id'));
        $validator = $this->store($post);
        if($validator){
            return view('post.create', compact('post'))->withErrors($validator);
        }
        return redirect('post-list');
    }
    public function delete($id){
        $post = Post::find($id);
        return view('post.delete', compact('post'));
    }
    public function deletePost()
    {
        Post::destroy(Input::get('id'));
        return redirect('post-list');
    }


	private function store($post)
	{
        $validator = Validator::make(Input::all(), array(
            'name' => 'required|min:4|max:500'
        ));
        if($validator->passes()){
            $post->fill(Input::all());
            $post->save();
            return null;
        }
        return $validator;
	}

    //----------- ATTESTATION PARAMS

    public function PostRequirements($id){
        $post = Post::with('Requirements')->find($id);

        return view('post.requirement', compact('post'));
    }

    public function UpdateRequirements(){
        $postId = Input::get('postId');
        $post = Post::find($postId);

        $requirements = json_decode(Input::get('requirements'));

        foreach($requirements as $value){
            if($value->isNew){
                $newReq = new PostRequirement();
                $newReq->FillFromObject($value);
                $post->Requirements()->save($newReq);
            }
            if($value->isUpdated && !$value->isNew){
                $newReq = PostRequirement::find($value->id);
                $newReq->FillFromObject($value);
                $newReq->save();
            }

            if($value->isDeleted){
                PostRequirement::destroy($value->id);
            }
        }
    }

}
