<?php namespace App\Http\Controllers;

use App\Models\RegistrationWishes;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('home');
	}

    public function login(){
        $authResult = null;
        return view('home.login', compact('authResult'));
    }

    public function loginPost(){
        $messages = $this->TryLogin(Input::get('login'), Input::get('password'));
        if(!$messages){
            return redirect("/");
        }
        $input = Input::all();
        return view('home.login', compact('messages', "input"));
    }

    private function TryLogin($login, $password){
        $credentionals = array(
            'login' => $login,
            'password' =>$password
        );

        $rules = array(
            'login'           => 'required',
            'password'        => 'required'
        );

        $validator = Validator::make($credentionals, $rules);
        if($validator->passes()){
            if (Auth::attempt($credentionals)) {
                return null;
            }
            $validator->getMessageBag()->add('credentionals', 'Неправильный логин или пароль');
        }

        return $validator->messages();
    }


    public function Register(){
        $user = new User;

        $roles = Role::whereNotIn('id', [Role::ADMIN, Role::USER])->get();

        return view('home.registration', compact('user', 'roles'));
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|string
     */
    public function RegisterPost(){
        if(Request::ajax()){
            return "laravel registration";
        }
        $rules = array(
            'first_name' => 'required|min:2|max:200',
            'last_name' => 'min:2|max:200',
            'login' => 'required|alpha_dash|unique:users|min:4|max:32',
            'password' => 'required|min:3|max:20'
        );
        $validator = Validator::make(Input::all(), $rules);
        $user = new User;
        $user->fill(Input::all());
        if($validator->passes()){
            $user->password = Hash::make($user->password);
            $user->save();

            $wishes = new RegistrationWishes();
            $wishes->comment = "";
            $wishes->roles = Input::get("wishesRoles");

            $user->RegistrationWishes()->save($wishes);

            $this->TryLogin($user->login, Input::get('password'));
            return redirect('/');
        }
        $roles = Role::whereNotIn('id', [Role::ADMIN, Role::USER])->get();
        return view('home.registration', compact('user', 'roles'))->withErrors($validator);
    }

    public function about(){
        return view('home.about');
    }
    public function contact(){
        return view('home.contact');
    }

    public function logout(){
        Auth::logout();
        return redirect("/");
    }
}
