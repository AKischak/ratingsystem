<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Post;
use Illuminate\Http\Request;

class TeacherController extends Controller {

	public function Cabinet(){
        $post = Post::first();
        return view('teacher.cabinet', compact('post'));
    }

}
