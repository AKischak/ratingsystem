<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Attestation;
use App\Models\ExpertGroup;
use App\Models\Post;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExpertController extends Controller {

    public function Cabinet(){
        $user = User::with('Expert')->find(Auth::id());

        $groups = $user->Expert->groups()->get()->lists('id');

        $man = Teacher::with('user')->first();
        $post = Post::first();

        $lst = Attestation::with('Results')->get();

        $notAttestaion = array();
        $attestaion = array();

        foreach($lst as $att){
            $isAtt = false;
            foreach ($att->Results as $res) {
                if($res->expert_id == $user->Expert->id){
                    $isAtt = true;
                    break;
                }
            }
            if($isAtt){
                $attestaion[] = $att;
            }else{
                $notAttestaion[] = $att;
            }
        }



        return view('expert.cabinet', compact('user', 'groups', 'attestaion', 'notAttestaion', 'man', 'post'));
    }

}
