<?php namespace App\Http\Controllers;

use App\Http\Requests;

use App\Models\Expert;
use App\Models\ExpertGroup;
use App\Models\Post;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class AttestationController extends Controller {

	public function index(){
        $post = Post::with('Requirements')->first();

        $man = Teacher::first();

        return view('attestation.index', compact('post', 'man'));
    }

    public function Results(){

        $post = Post::with('Requirements')->first();

        $man = Teacher::first();
        return view('attestation.results', compact('post', 'man'));
    }
}
