<?php namespace App\Http\Controllers;

use DB;
use App\Http\Requests;
use App\Models\User as User;
use App\Models\Post as Post;
use App\Http\Controllers\Controller;
use App\Utils;
use App\Utils\GlobalConst as GlobalConst;

use Illuminate\Http\Request;

class SinglePageController extends Controller
{

    //

    public function index()
    {

        $tags = explode(' ', GlobalConst::GetLorem());

        foreach ($tags as $key => $value) {
            if (strlen($value) < 10) {
                unset($tags[$key]);
            }
        }

        $appName = "SINGLE PAGE APPLICATION";
        $secondName = "with Laravel";

        //$tags = ['home', 'singlepage', 'laravel', 'blade', 'engine'];
        return view('singlepage.index', compact('appName', 'tags', 'secondName'));
    }


    public function DatabaseTesting($action = null)
    {
        $appName = "SINGLE PAGE APPLICATION";
        $data = 'must define';
        switch ($action) {
            case 'select':
                $data = $this->selectData();
                break;
            case 'delete':
                $data = $this->deleteData();
                break;
            case 'insert':
                $data = $this->insertData();
                break;
            case 'builder':
                $data = $this->QueryBuilder();
                break;
            case 'elo':
                $data = $this->Eloquent();
                break;
            default:
                $data = "Undefined action";
                break;
        }

        return view('singlepage.database', compact('appName', 'data', 'action'));
    }

    private function selectData()
    {
        //$data = DB::select('select name from user');
        $n = rand(5, 40);

        $tags = explode('.', GlobalConst::GetLorem());
        $text= $tags[rand(0, count($tags) - 1)];

        $data = Post::all()->first();

        $post = Post::create(array('text'=>$text));
        $us = User::find(602);
        $post->user()->associate($us);
        $post->save();

        return $us->posts()->get()->toArray();
    }

    private function insertData(){
        $n = rand(5, 40);

        $tags = explode(' ', GlobalConst::GetLorem());
        for($i = 0; $i<$n; $i++){
            $name = $tags[rand(0, count($tags) - 1)];
            $user = User::create(array('name'=>$name));
        }


        return "Записей: ".User::all()->count().', было добавлено '.$n;
    }

    private function deleteData(){
        $n = rand(0, 15);
        $old = DB::select('select count(*) as result from user')[0]->result;
        User::whereRaw('length(name) < ?', array($n))->delete();//DB::delete('delete from user where LENGTH(name) > ?', array($n));
        $now = DB::select('select count(*) as result from user')[0]->result;
        return "Записей было $old стало $now";
    }

    private function QueryBuilder(){
        $data = DB::table('user')->remember(1)->get();
        return $data;
    }

    private function Eloquent(){
        try{
            $data = [];
            $i = 0;
            User::whereRaw('LENGTH(name) > ?', array(2))->chunk(30, function($chunk)use(&$data, &$i){
                $i++;
                foreach($chunk as $value){
                    $data[$i][] = $value->name;
                }
            });
        }catch(\Exception  $e){
            $data = "Возникло исключение ".$e->getMessage();
        }

        return $data;
    }

}
