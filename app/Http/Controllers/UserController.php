<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Expert;
use App\Models\Role;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{

    public function UserManager()
    {

        $users = User::where('confirmed', '=', '1')->whereNotIn('id', [Role::ADMIN])->get();

        $roles = Role::select(array('id', 'name'))->get();

        return view('user.users', compact('users', 'roles'));
    }

    public function UserDetail($id)
    {

        $model = User::find($id);

        $roles = Role::select(array('id', 'name'))->whereNotIn("id", [Role::USER])->get();

        return view('user.userDetails', compact('roles', 'model'));
    }

    public function UserRoleUpdate()
    {
        $roleIds = json_decode(Input::get("roles"));
        $roleIds[] = Role::USER;

        $user = User::with('roles')->find(Input::get("userId"));

        $user->roles()->detach();


        foreach ($roleIds as $roleId) {
            $user->roles()->attach($roleId);
        }

        $userRoles = $user->roles()->select(array('roles.id'))->get();

        if (in_array(Role::EXPERT, array_pluck($userRoles, 'id'))) {
            if ($user->Expert == null) {
                $user->Expert()->save(new Expert);
            }
        } else {
            if ($user->Expert != null) {
                $user->Expert()->delete();
            }
        }
        if (in_array(Role::TEACHER, array_pluck($userRoles, 'id'))) {
            if ($user->Teacher == null) {
                $user->Teacher()->save(new Teacher);
            }
        } else {
            if ($user->Teacher != null) {
                $user->Teacher()->delete();
            }
        }

        return Input::get("roles");
    }


    public function NewUsers()
    {
        $users = User::with('RegistrationWishes')->where('confirmed', '=', 0)->get();

        $allWishes = [];
        foreach ($users as $user) {
            $allWishes[] = $user->RegistrationWishes;
        }

        $allRoles = Role::whereNotIn('id', [Role::ADMIN, Role::USER])->get();

        return view('user.confirm', compact("users", 'allWishes', 'allRoles'));
    }

    public function TryConfirmUser()
    {
        $user = User::find(Input::get("userId"));
        $user->confirmed = true;
        $user->save();
        return $user->id;
    }

    public function UserRemove($id)
    {
        User::destroy($id);
        return redirect("/users");
    }
}
