<?php namespace App\Http\Controllers;

use App\Http\Requests;

use App\Models\Attestation;
use App\Models\Expert;
use App\Models\ExpertGroup;
use App\Models\Post;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ExpertGroupController extends Controller {

	public function index()
	{
        $groups = ExpertGroup::all();


        return view('expertgroup.index', compact('groups'));
	}

	public function create()
	{
        $group = new ExpertGroup();
        return view('expertgroup.create', compact('group'));
	}

    public function createPost()
    {
        $group = new ExpertGroup;
        $validator = $this->store($group);
        if($validator){
            return view('expertgroup.create', compact('group'))->withErrors($validator);
        }
        return redirect('expert-group-list');
    }

    public function update($id){
        $group = ExpertGroup::find($id);
        return view('expertgroup.update', compact('group'));
    }
    public function updatePost()
    {
        $group = ExpertGroup::find(Input::get('id'));
        $validator = $this->store($group);
        if($validator){
            return view('expertgroup.create', compact('group'))->withErrors($validator);
        }
        return redirect('expert-group-list');
    }
    public function delete($id){
        $group = ExpertGroup::find($id);
        return view('expertgroup.delete', compact('group'));
    }
    public function deletePost()
    {
        ExpertGroup::destroy(Input::get('id'));
        return redirect('expert-group-list');
    }


	private function store($group)
	{
        $validator = Validator::make(Input::all(), array(
            'name' => 'required|min:4|max:500'
        ));
        if($validator->passes()){
            $group->fill(Input::all());
            $group->save();
            return null;
        }
        return $validator;
	}

    public function ExpertModify($id){
        $group = ExpertGroup::with('experts')->find($id);
        $groupExperts = $group->experts;
        $allExperts = Expert::with('User')->get();

        return view('expertgroup.modifyExperts', compact('group', 'groupExperts', 'allExperts'));
    }

    public function SaveExperts(){
        $expIds = json_decode(Input::get("experts"));

        $group = ExpertGroup::find(Input::get("groupId"));
        $group->experts()->detach();

        foreach($expIds as $id){
            $group->experts()->attach($id);
        }

        return $group->id;
    }

    public function TeacherAttestation($id){
        $group = ExpertGroup::with('Attestations')->find($id);
        $attestations = $group->Attestations;
        $allTeachers = Teacher::with('User')->get();

        $posts = Post::all();
        return view('expertgroup.modifyAttestation', compact('group', 'attestations', 'allTeachers', 'posts'));
    }

    public function SaveAttestation(){
        $attestations = json_decode(Input::get("attestation"));

        $group = ExpertGroup::find(Input::get("groupId"));

        $result = array();
        foreach($attestations as $att){
            if(isset($att->isNew) && $att->isNew){
                $new = new Attestation;
                $new->teacher_id = $att->teacherId;
                $new->post_id = $att->postId;
                $group->Attestations()->save($new);
                $result[] = $new;
               // Log::info("SAVE NEW ATTESTATION! $new->id, post: ");
            }else if(isset($att->isDeleted) && $att->isDeleted){
                Attestation::destroy($att->id);
                Log::info("REMOVE ATTESTATION! ".$att->id);
            }
        }

        return json_encode($result);
    }

}
